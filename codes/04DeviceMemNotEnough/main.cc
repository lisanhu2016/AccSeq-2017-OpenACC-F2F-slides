#include <iostream>
#include <cstring>

using namespace std;

// #define ONE_GB (1<<30)
#define ONE_GB (1<<29)
#define SET_SIZE 6L

int main(int argc, char const *argv[]) {
	char *store = new char[SET_SIZE * ONE_GB];
	char *partial = new char[ONE_GB];

	for (size_t i = 0; i < SET_SIZE; ++i) {
		std::cout << "Begin set " << i << '\n';
		#pragma acc kernels
		for (size_t j = 0; j < ONE_GB; j++) {
			size_t id = i * SET_SIZE + j;
			if (id < (SET_SIZE * ONE_GB)) {
				partial[id] = id % 2;
			}
		}

		memcpy(store + i * ONE_GB, partial, ONE_GB);
	}

	for (size_t i = 0; i < (SET_SIZE * ONE_GB); ++i) {
		if (store[i] != i % 2) {
			std::cerr << "Wrong value for 1st kernels" << '\n';
		}
	}

	std::cout << "Done checking 1st kernels" << '\n';

#pragma acc kernels
	for (size_t i = 0; i < SET_SIZE * ONE_GB; ++i) {
		store[i] = i % 2;
	}

	for (size_t i = 0; i < (SET_SIZE * ONE_GB); ++i) {
		if (store[i] != i % 2) {
			std::cerr << "Wrong value for 2nd kernels" << '\n';
		}
	}
	std::cout << "Done checking 2nd kernels" << '\n';
	return 0;
}
