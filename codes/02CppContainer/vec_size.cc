#include <iostream>
#include <vector>

using namespace std;

#define NUM_ELE 100
// #define FILL_VALUE 1

int main(int argc, char const *argv[]) {
	vector<int> v;
	int *store = new int[NUM_ELE];

	for (size_t i = 0; i < NUM_ELE; ++i) {
		v.push_back(i);
	}

	#pragma acc kernels loop independent
	for (size_t i = 0; i < v.size(); ++i) {
		store[i] = v[i] * 2;
	}

	std::cout << "Begin checking..." << '\n';

	for (size_t i = 0; i < NUM_ELE; ++i) {
		if (store[i] != v[i] * 2) {
			std::cerr << "Wrong value for vector dynamic size" << '\n';
		}
	}

	std::cout << "Done checking" << '\n';

	return 0;
}
