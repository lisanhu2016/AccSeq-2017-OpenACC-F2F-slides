#include <iostream>
#include <cstdlib>

using namespace std;

#define NUM_ELE 100

class Pair {
public:
	int a, b;
	#pragma acc routine
	Pair() {}
	void init() {a = b = 1;}
	~Pair() { a = b = 0; }
};

void do_sth(size_t ii, void *space) {
	Pair *store = (Pair *)space;
	Pair stack[10];
	for (size_t i = 0; i < 10; i++) {
		store[i] = stack[i];
		store[i].init();
	}
}

int main(int argc, char const *argv[]) {
	void *p = calloc(NUM_ELE * 10, sizeof(Pair));
	Pair *vals = (Pair *)p;

#pragma acc kernels copyout(vals[:NUM_ELE * 10])
	for (size_t i = 0; i < NUM_ELE; ++i) {
		do_sth(i, vals + i * 10);
	}

std::cout << "Begin checking" << '\n';
	int *check = (int *) p;
	for (size_t i = 0; i < NUM_ELE * 20; ++i) {
		if (check[i] != 1) {
			cerr << "Wrong result!!!" << endl;
		}
	}
	std::cout << "Done checking" << '\n';
	return 0;
}
