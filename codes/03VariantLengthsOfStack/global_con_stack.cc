#include <iostream>

using namespace std;

#define NUM_ELE 100

const int sz = 10;

int stack_test() {
	int nums[sz];
	int sum = 0;
	for (size_t i = 0; i < sz; ++i) {
		nums[i] = i + 1;
	}

	for (size_t i = 0; i < sz; ++i) {
		sum += nums[i];
	}
	return sum;
}

int main(int argc, char const *argv[]) {
	int vals[NUM_ELE];

#pragma acc kernels
	for (size_t i = 0; i < NUM_ELE; ++i) {
		vals[i] = stack_test();
	}

	for (size_t i = 0; i < NUM_ELE; ++i) {
		if (vals[i] != 55) {
			cerr << "Value error" << '\n';
		}
	}
	return 0;
}
