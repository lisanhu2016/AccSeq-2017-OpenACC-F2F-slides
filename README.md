# AccSeq 2017 Summer OpenACC F2F meeting slides

__By: Sanhu Li, Dr. Sunita Chandrasekaran__

## Introduction

This is a 15 min presentation, talking about user experience using OpenACC in our projects, problems met, solutions tried, problems not solved, proposed features, future challenges asking for suggestions, interested in progress of supporting c++11/14/17 plan on 20, etc.

## Files

This repository contains the slides, along with some code snippets for the problems. The code snippets are created to test whether newest PGI compiler has solved the proposed problems, and reproduce the errors got from the compiler.

- *AccSeq OpenACC User Feedback.pptx* is the presentation slides.
- *codes/* is the folder containing all the small code snippets projects for the presentation
- *README.md* is this file, a description of this repository.

## Tested Platform
Hardware configuration:
- Intel I7-7700HQ
- NVIDIA GTX 1050 (4GB device memory)

Software configuration:
- Ubuntu Gnome 16.04.2 LTS (NVIDIA driver runfile installation with problems on 17.04)
- CUDA 8.0
- PGI 17.4
- GCC 5.4

## Contacts

If you have any questions, please contact [Sanhu Li](mailto:lisanhu@udel.edu)
